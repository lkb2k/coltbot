import unittest
import app
from transformers import GPT2TokenizerFast
from pathlib import Path


class TestCalculations(unittest.TestCase):
    def test_summarize(self):
        txt = Path('too-long.txt').read_text()
        tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
        tokens = len(tokenizer(txt)['input_ids'])
        while tokens > 4000:
            txt = '\n'.join(txt.split('\n')[1:])
            tokens = len(tokenizer(txt)['input_ids'])
        print(app.get_summery(txt))