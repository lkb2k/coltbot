# coltbot

Coltbot is a slackbot that sumerizes the most recent chats in a given slack conversation 

## Installation 
1. Setup a slack bot with the following permissions:
```
app_mentions:read
channels:history
channels:read
chat:write
groups:history
groups:read
groups:write
users:read
```

2. Register of a OpenAPI token from opeinai https://openai.com/

3. Setup up the following environment variables
```
OPENAPI_KEY=[Key from OpenAPI]
SLACK_KEY=[Slack bot key]
BOT_USER=[Your Slack Bot UserID]
```

4. build the docker image, and run the bot. 

5. Update your bot configuration in slack to point to you callback host. For local development consider using ngrok for tunnneling 

## Usage
Once instead, just invite colt bot to a channel and then tag it. As soon as it sees the mention - it will give you a summery. 