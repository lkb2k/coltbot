import json
import logging
import os
import random
import threading

import openai
from dotenv import load_dotenv
from flask import Flask, request
# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
# Do some pre-analysis before sending to OpenAI
from transformers import GPT2TokenizerFast

load_dotenv()

openai.api_key = os.environ["OPENAPI_KEY"]
slack_key = os.environ["SLACK_KEY"]
bot_user = os.environ["BOT_USER"]

# WebClient instantiates a client that can call API methods
# When using Bolt, you can use either `app.client` or the `client` passed to listeners.
client = WebClient(token=slack_key)
logger = logging.getLogger(__name__)

# Just used to check that the sample text
# isn't over the limit for OpenAI's API
tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")

# will prob lead to a memory issue one day
users = {}


# I kept running into issues with the slack message
# history being too big.  I wasn't really sure how
# they count 'tokens' so I just grabbed the first
# tokenizer that seemed reasonable from their API
# docs
#
# Trim the input until it fits into the max
# token count for the API call. has to be
# under 4097.  Obviously this will result in
# an infinite loop for a really long string
# with no newline characters, will see if that
# happens in practice
def redact(text, max_tokens):
    trimmed_text = text
    tokens = len(tokenizer(trimmed_text)['input_ids'])
    while tokens > max_tokens:
        trimmed_text = '\n'.join(trimmed_text.split('\n')[1:])
        tokens = len(tokenizer(trimmed_text)['input_ids'])
    return trimmed_text


def snarkify(text):
    styles = [
        "donald trump", "cher horowitz", "jfk", "dorothy parker", "chaucer", "oscar wilde", "aristotle"
    ]
    style = random.choice(styles)
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt="rephrase '" + text + "' as if spoken by " + style,
        temperature=0.7,
        max_tokens=100,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=1
    )
    return response['choices'][0]["text"]


def get_summery(text):
    try:
        redacted = redact(text, 3500)
        print("~~~~~~~  INPUT ~~~~~~~ ")
        print(redacted)
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt=redacted + "\n\nTl;dr",
            temperature=0.7,
            max_tokens=150,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=1
        )
        print("~~~~~~~  OUTPUT ~~~~~~~~")
        print(response)
        return response['choices'][0]["text"]
    except Exception as e:
        print(e)
        return f"OpenAI API choked: {e}"


def lookupuser(user):
    if user in users:
        return users[user]["real_name"]

    result = client.users_info(user=user)
    if "user" in result:
        users[user] = result["user"]
        return result["user"]["real_name"]

    return "NEMO"


def get_convo_summery(conversation_id, thread_id):
    try:
        print(conversation_id + '|' + (thread_id if thread_id else ""))
        conversation_history = []

        if thread_id is None:
            result = client.conversations_history(channel=conversation_id)
            conversation_history = result["messages"]
        else:
            result = client.conversations_replies(channel=conversation_id, ts=thread_id)
            conversation_history = result["messages"]

        history = []

        convo_text = ""
        for message in conversation_history:
            msg_thread_id = message["thread_ts"] if "thread_ts" in message else None
            msg_ts = message["ts"]

            if message["user"] != bot_user:
                username = lookupuser(message["user"])
                msg = username + ":" + message["text"]
                history.append(msg)

            # Check to see if the bot has already summarized
            # and only include messages after that point.
            # Unless the summary was in a different thread
            elif thread_id == msg_thread_id or msg_ts == msg_thread_id:
                # TODO: Figure out how to identify bot responses
                #  that are not summaries
                break

        # Don't bother summarizing if there isn't much message history
        if len(history) < 5:
            return snarkify("There's not much to summarize. Can't you just read it?")

        # flip conversation history
        history.reverse()

        convo_text = "\n".join([str(item) for item in history])
        print("=====sumerizing========")
        return get_summery(convo_text)

    except SlackApiError as e:
        print(f"Error: {e}")


# Threaded wrapper for request
def handle_mention_request(channel, thread=None):
    summery = get_convo_summery(channel, thread)
    result = client.chat_postMessage(
        channel=channel,
        text=summery,
        thread_ts=thread
        # You could also use a blocks[] array to send richer content
    )


app = Flask(__name__)


# Handles slack callbacks, specfically challenge and app_mention
@app.route('/', methods=['POST'])
def process_request():
    request_json = request.get_json()

    print(request_json)

    if ("event" in request_json and request_json["event"]["type"] == "app_mention"):
        channel = request_json["event"]["channel"]
        slack_thread = None
        if "thread_ts" in request_json["event"]:
            slack_thread = request_json["event"]["thread_ts"]

        # launch thread since calls take time, and slack expects quick response else spams you.
        thread = threading.Thread(target=handle_mention_request, kwargs={'channel': channel, 'thread': slack_thread})
        thread.start()

        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}

    return request_json["challenge"]


@app.route('/', methods=['GET'])
def healthcheck():
    return 'OK'


if __name__ == '__main__':
    app.run(debug=True)
    # print (get_summery(""))
